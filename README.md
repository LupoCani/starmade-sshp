SSHP Mod for Starmade. All rights reserved. All rights rights to Starmade reserved by Schine GmbH.


Mod intended to create a new variable, called "subsystem HP" (SSHP), for every system on board an entity. In this implementation, a system is considered equivalent to an ElementCollectionManager. When a system takes damage, SSHP drops faster than the system is physically destroyed, and limits the performance of the system to the remaining SSHP, rather than the remaining blocks, where the former is always more restrictive than the latter. Design document [here](https://docs.google.com/document/d/1mSGS_y1AyYHSVltSee8vxDZvGPRQpiH0cpZMo-Hv9l4/edit?usp=sharing).

**SSHPCounter (SSHPC)** - tracks the SSHP of a particular ECM.

**SSHPCounterController (SSHPCC)**- manages the SSHPCounters of a particular entity (SegmentContoller).

**SHPCControllerManager (SSHPCCM)** - single instance, manages all SSHPCCs in the game.