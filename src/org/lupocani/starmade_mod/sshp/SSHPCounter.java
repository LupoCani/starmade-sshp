package org.lupocani.starmade_mod.sshp;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import org.lupocani.starmade_mod.sshp.network.SSHPPacket;
import org.lupocani.starmade_mod.sshp.network.SSHPSendable;
import org.lupocani.starmade_mod.sshp.persist.SerialSSHPC;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;

import java.lang.Math;

/** SSHP counter
 * Counts SSHP for a single system.
 * For this mod, each ElementCollectionManager is considered one system.
 */
public class SSHPCounter implements SSHPSendable {
    protected float subSystemHP;
    protected float maxSSHP;
    protected float damageFactor;
    protected float excessDamage;
    protected boolean isDamaged;

    public final short blockID;
    public final String systemName;

    final private UsableElementManager<?,?,?> elementManager;
    final private SSHPCounterController controller;

    private boolean syncStatus;
    private boolean networkSemaphore;
    public final boolean isOnServer;

    public SSHPCounter(UsableElementManager<?,?,?> UEM, SSHPCounterController controller) {
        //super(SSHPUtils.getUEMBlockID(UEM), UEM.getManagerName());
        this.blockID = SSHPUtils.getUEMBlockID(UEM);
        this.systemName = UEM.getManagerName();

        subSystemHP = 1;
        isDamaged = false;
        damageFactor = 5;
        excessDamage = 0;

        this.elementManager = UEM;
        this.networkSemaphore = false;
        this.isOnServer = getSegmentController().isOnServer();
        this.controller = controller;
        this.syncStatus = false;

        onShipReboot(false);
    }

    public SSHPCounter(SerialSSHPC unloaded, UsableElementManager<?,?,?> UEM, SSHPCounterController controller) {
        this.subSystemHP = unloaded.subSystemHP;
        this.maxSSHP = unloaded.maxSSHP;
        this.excessDamage = unloaded.excessDamage;
        this.damageFactor = unloaded.damageFactor;
        this.isDamaged = unloaded.isDamaged;
        this.systemName = unloaded.systemName;

        this.elementManager = UEM;
        this.isOnServer = getElementManager().isOnServer();
        this.controller = controller;
        this.syncStatus = false;

        if (unloaded.blockID > 0)
            this.blockID = unloaded.blockID;
        else {
            short blockID = -1;
            for (ElementInformation info : ElementKeyMap.getInfoArray())
                if (ElementInformation.getKeyId(info.getId()).equals(unloaded.blockName))
                    blockID = info.getId();

            this.blockID = blockID;
        }

    }

    //Game interface methods

    public UsableElementManager<?,?,?> getElementManager() {
        return elementManager;
    }

    public SegmentController getSegmentController() {
        return getElementManager().getSegmentController();
    }

    private void updateDamageFactor() {
        double disintegrity = Math.max(1, -getElementManager().lowestIntegrity);
        double size = maxSSHP;

        double effSizeExponent = Main.config.effSizeExponent.getValue();
        double disintegrityExponent = Main.config.disintegrityExponent.getValue();
        double sizeRebate = Main.config.sizeRebate.getValue();
        double startFactor = Main.config.sizeFactorStart.getValue();
        double endFactor = Main.config.sizeFactorEnd.getValue();

        startFactor = Math.max(startFactor, endFactor);

        double rebSize = Math.max(size-sizeRebate, 1);
        double effectiveSizeBase = Math.pow(rebSize, effSizeExponent);
        double baseFactor =  1/((startFactor-endFactor) * (effectiveSizeBase/rebSize) + endFactor);
        this.damageFactor = (float) (baseFactor * Math.pow(disintegrity, disintegrityExponent));

        if (subSystemHP > 0)
            return;

        double excessFactor = Main.config.excessDamage.getValue();
        boolean flatExcess = Main.config.flatExcessDamage.getValue();

        if (flatExcess)
            this.damageFactor = 1.0f + (float) excessFactor;
        else
            this.damageFactor = 1 + (damageFactor-1) * (float) excessFactor;
    }

    //Physical event methods

    public void onBlockKill() {
        Main.print(0,"Killing block, sshp is " + subSystemHP);
        //generalUpdate();
        isDamaged = true;
        if (0 < subSystemHP)
            adjustSSHP(-damageFactor, false); //Do not clamp SSHP upwards, to prevent double-counting.
            //(In case > damageFactor blocks were destroyed in one tick. Possibly an unnecessary caution.)
        else
            excessDamage += damageFactor;

        scheduleNetworkUpdate();

        Main.print(0,"Block killed, " + getElementManager().getManagerName()+" sshp is " +
                subSystemHP + "/" + maxSSHP + " = " + getFractSSHP() + ", net is " + getNetSSHP());
    }

    public boolean onBlockKill(SegmentPiece sPiece) {
        if (containsSegmentPiece(sPiece)){
            onBlockKill();
            return true;
        }
        else return false;
    }

    public void onBlockRepair(float power) {
        adjustSSHP(power, true);
        excessDamage = Math.max(0, excessDamage-power);
        if (subSystemHP == maxSSHP)
            isDamaged = false;
    }

    public void generalUpdate() {
        updateMaxSSHP();
        updateDamageFactor();
        if (!isDamaged)
            resetSSHP();

        scheduleNetworkUpdate();
    }

    public void onShipReboot(boolean failed) {
        updateMaxSSHP();
        isDamaged = failed;
        if (failed)
            subSystemHP = 0;

        generalUpdate();
    }

    private void updateMaxSSHP() {
        this.maxSSHP = Math.max(getElementManager().totalSize, 0);
        Main.print(0,"("+getSegmentController().getId()+":"+systemName+" MSSHP is "+this.maxSSHP+")");
    }

    //Utility methods

    private void resetSSHP() {
        subSystemHP = maxSSHP;
        excessDamage = 0;
    }

    private void clampSSHP(boolean max) {
        if (maxSSHP > 0)
            if (max)
                subSystemHP = Math.max(0, Math.min(maxSSHP, subSystemHP));
            else
                subSystemHP = Math.max(0, subSystemHP);
        else
            subSystemHP = 0;
    }

    private void adjustSSHP(float change, boolean clamp) {
        subSystemHP += change;
        clampSSHP(clamp);
    }

    public float getFractSSHP() {
        if (controller.isRebooting())
            return 0;

        if (isDamaged)
            return Math.min(1, subSystemHP / Math.max(1, maxSSHP));

        return 1;
    }

    public float getNetSSHP() {
        return subSystemHP - excessDamage;
    }

    public boolean containsSegmentPiece(SegmentPiece segmentPiece) {
        return segmentPiece.getInfo().id == blockID;
    }

    //Networking methods

    public void updateFromNetwork(float sshp, float excess, boolean damaged) {
        subSystemHP = sshp;
        isDamaged = damaged;
        excessDamage = excess;
        if ( !isOnServer && !syncStatus) {
            syncStatus = true;
            SSHPUtils.forceUpdateAllECMs(controller.segmentController,true);
        }
        Main.print(0,"SSHP package processed. ("+controller.segmentController.getId()+"/"+this.blockID+"/"+damaged+"/"+sshp+")");
    }

    @Override
    public void sendNetworkUpdate() {
        if (!isOnServer) return;
        networkSemaphore = false;

        int entID = getSegmentController().getId();

        SSHPPacket sshpPacket = new SSHPPacket(entID, blockID, subSystemHP, excessDamage, isDamaged);

        getSegmentController().getRemoteSector();
        for (PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) {
            PacketUtil.sendPacket(player, sshpPacket);
            Main.print(0,"SSHP package sent. ("+entID+"/"+blockID+":"+subSystemHP+")");
        }
    }

    @Override
    public void sendNetworkUpdatePrivate(PlayerState player) {
        PacketUtil.sendPacket(player, new SSHPPacket(getSegmentController().getId(), blockID, subSystemHP, excessDamage, isDamaged));
    }

    public void scheduleNetworkUpdate() {
        if (networkSemaphore || !isOnServer) return;
        networkSemaphore = true;
        Main.scheduleNetworkUpdate(this);
    }

    public SerialSSHPC getSerial() {
        return new SerialSSHPC(subSystemHP, maxSSHP, excessDamage, damageFactor, isDamaged, blockID, systemName);
    }
}
