package org.lupocani.starmade_mod.sshp.utils;

import api.common.GameServer;
import api.listener.events.calculate.ShieldCapacityCalculateEvent;
import api.listener.events.calculate.ShieldRegenCalculateEvent;
import api.listener.events.systems.ThrustCalculateEvent;
import api.listener.events.weapon.AnyWeaponDamageCalculateEvent;
import api.listener.events.weapon.UnitModifierHandledEvent;
import api.utils.game.SegmentControllerUtils;
import api.utils.gui.GUIMenuPanel;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.SSHPCCManager;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.lupocani.starmade_mod.sshp.UI.SSHPMenuPanel;
import org.lupocani.starmade_mod.sshp.api.ModSystemIdCallback;
import org.lupocani.starmade_mod.sshp.api.SSHPModInterface;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SegmentControllerHpController;
import org.schema.game.common.controller.SegmentControllerHpControllerInterface;
import org.schema.game.common.controller.elements.*;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamElementManager;
import org.schema.game.common.controller.elements.combination.modifier.BeamUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.MissileUnitModifier;
import org.schema.game.common.controller.elements.combination.modifier.Modifier;
import org.schema.game.common.controller.elements.combination.modifier.WeaponUnitModifier;
import org.schema.game.common.controller.elements.missile.MissileElementManager;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.controller.elements.shield.capacity.ShieldCapacityCollectionManager;
import org.schema.game.common.controller.elements.shield.regen.ShieldRegenCollectionManager;
import org.schema.game.common.controller.elements.thrust.ThrusterCollectionManager;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;
import org.schema.game.common.controller.elements.weapon.WeaponElementManager;
import org.schema.game.common.data.player.PlayerControlledTransformableNotFound;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

public class SSHPUtils {
    public static ArrayList<ElementCollectionManager<?,?,?>> getAllCollectionManagers(ManagedUsableSegmentController<?> ent) {
        ArrayList<ElementCollectionManager<?,?,?>> ecms = new ArrayList<ElementCollectionManager<?,?,?>>();

        for (ManagerModule<?, ?, ?> module : ent.getManagerContainer().getModules()) {
            if (module instanceof ManagerModuleCollection) {

                for (Object cm : ((ManagerModuleCollection<?, ?, ?>) module).getCollectionManagers()) {
                    ecms.add((ElementCollectionManager<?, ?, ?>) cm);
                }
            } else if (module instanceof ManagerModuleSingle) {
                ElementCollectionManager<?, ?, ?> cm = ((ManagerModuleSingle<?, ?, ?>) module).getCollectionManager();
                ecms.add(cm);
            }
        }
        return ecms;
    }

    public static ArrayList<ElementCollectionManager<?,?,?>> getAllCollectionManagers(UsableElementManager<?, ?, ?> uem) {
        ArrayList<ElementCollectionManager<?,?,?>> ecms = new ArrayList<ElementCollectionManager<?,?,?>>();
        ArrayList<ElementCollectionManager<?,?,?>> ecms_base =
                getAllCollectionManagers((ManagedUsableSegmentController<?>) uem.getSegmentController());

        for (ElementCollectionManager<?,?,?> ecm : ecms_base) {
            UsableElementManager<?, ?, ?> uemToCheck = ecm.getElementManager();
            if (uemToCheck == uem) {
                ecms.add(ecm);
            }
        }

        return ecms;
    }

    public static ArrayList<UsableElementManager<?,?,?>> getAllElementManagers(ManagedUsableSegmentController<?> ent) {
        ArrayList<UsableElementManager<?,?,?>> uems = new ArrayList<>();

        for (ManagerModule<?, ?, ?> module : ent.getManagerContainer().getModules()) {
            uems.add(module.getElementManager());
        }
        return uems;
    }

    public static short getSimlpleCollectionBlockID(ElementCollectionManager<?,?,?> elementCollectionManager) {
        if (elementCollectionManager.isDetailedElementCollections())
            return -2;

        if (elementCollectionManager instanceof ThrusterCollectionManager) {
            return 8; //Find thruster ID
        }

        return -1;
    }

    public static short getUEMBlockID(UsableElementManager<?,?,?> UEM) {
        if (UEM instanceof ThrusterElementManager) return SSHPids.THRUST; //Thruster ID

        if (UEM instanceof WeaponElementManager) return SSHPids.AMC; //Cannon barrel ID

        if (UEM instanceof DamageBeamElementManager) return SSHPids.BEAM; //Damage beam ID

        if (UEM instanceof MissileElementManager) return SSHPids.MISSILE; //Missile tube ID

        //if (UEM instanceof VoidElementManager<ShieldRegenUnit, ShieldRegenCollectionManager>)
        if (UEM instanceof VoidElementManager) {
            ElementCollectionManager<?,?,?> manager = ((VoidElementManager<?, ?>) UEM).getCollection();
            if (manager instanceof ShieldRegenCollectionManager) return SSHPids.SHIELD_REG;
            if (manager instanceof ShieldCapacityCollectionManager) return SSHPids.SHIELD_CAP;


        }

        //I wish there was a less ugly way to do the above.

        //For reporting the block id of other mods' UEMs, assuming they've been registered.
        return SSHPModInterface.getModUEMId(UEM);
    }

    public static SegmentController getSegmentControllerbyIDString(String string) {
        //A terrible terrible hack. There must be a less ugly way to do this.
        Map<String, SegmentController> map = GameServer.getServerState().getSegmentControllersByName();

        for (String name : map.keySet()) {
            SegmentController segmentController = map.get(name);

            if (string.equalsIgnoreCase(segmentController.getUniqueIdentifier())) {
                return segmentController;
            }
        }
        return null;
    }

    public static ManagedUsableSegmentController<?> getMUSControllerByIdString(String string) {
        SegmentController segmentController = getSegmentControllerbyIDString(string);
        if (segmentController instanceof ManagedUsableSegmentController<?>)
            return (ManagedUsableSegmentController<?>) segmentController;
        else
            return null;
    }

    public static int intIdFromStringId(String stringId) {
        SegmentController segmentController = getSegmentControllerbyIDString(stringId);
        if (segmentController == null)
            return Integer.MAX_VALUE;
        else return segmentController.getId();
    }

    public static SegmentController getPilotedEntityFromPlayer(PlayerState playerState) {
        try {
            return (SegmentController) playerState.getFirstControlledTransformable();
        } catch (PlayerControlledTransformableNotFound | ClassCastException e) {
            return null;
        }
    }

    public static void forceOverheat(ManagedUsableSegmentController<?> controller) {
        SegmentControllerHpControllerInterface hpControllerInterface = controller.getHpController();
        if (!(hpControllerInterface instanceof SegmentControllerHpController)) return;

        ReactorTree tree = controller.getManagerContainer().getPowerInterface().getActiveReactor();

        try {
            Field f = tree.getClass().getDeclaredField("hp");
            f.setAccessible(true);
            f.set(tree, 0L);
        } catch (NoSuchFieldException | IllegalAccessException | NullPointerException e) {
            Main.print(3, "Could not force overheat.");
            e.printStackTrace();
        }

        ((SegmentControllerHpController) hpControllerInterface).triggerOverheating();
    }

    public static void handleThrust(SSHPCCManager manager, ThrustCalculateEvent event) {
        ThrusterElementManager elementManager = event.getThrusterElementManager().getElementManager();
        SSHPCounterController sshpcController = manager.getSSHPCC(elementManager.getSegmentController());

        Main.print(0,"Thrust TBD.");

        if (sshpcController != null) {
            event.setThrust(event.getCalculatedThrust() * sshpcController.getSystemCapacity(elementManager));

            Main.print(0,"Thrust adjusted by " + sshpcController.getSystemCapacity(elementManager));
        } else {
            Main.print(0,"No matching SSHPCC.");
        }
    }

    public static void handleWeapon(SSHPCCManager manager, AnyWeaponDamageCalculateEvent event) {
        Main.print(0,"Normal damage calculated.");

        SSHPCounterController sshpCounterController = manager.getSSHPCC(event.customOutputUnit.getSegmentController());

        if (sshpCounterController != null) {
            event.damage *= sshpCounterController.getSystemCapacity(
                    event.customOutputUnit.elementCollectionManager.getElementManager());
        }
    }

    public static void handleWeaponCombo(SSHPCCManager manager, UnitModifierHandledEvent event) {
        Main.print(0,"Combo damage calculated.");
        Modifier<?,?> unitModifier =  event.weaponUnitModifier;
        ElementCollectionManager<?,?,?> elementCollectionManager = event.controlBlockElementCollectionManager;
        SSHPCounterController sshpCounterController = manager.getSSHPCC(elementCollectionManager.getSegmentController());
        float fractSSHP;
        if (sshpCounterController != null) {
            fractSSHP = sshpCounterController.getSystemCapacity(elementCollectionManager.getElementManager());
        } else return;

        if (unitModifier instanceof WeaponUnitModifier) {
            ((WeaponUnitModifier) unitModifier).outputDamage *= fractSSHP;
        }
        else if (unitModifier instanceof BeamUnitModifier<?>) {
            ((BeamUnitModifier<?>) unitModifier).outputDamagePerHit *= fractSSHP;
        }
        else if (unitModifier instanceof MissileUnitModifier<?>) {
            ((MissileUnitModifier<?>) unitModifier).outputDamage *= fractSSHP;
        }
    }

    public static void handleShieldCap(SSHPCCManager sshpccManager, ShieldCapacityCalculateEvent event) {
        ShieldCapacityCollectionManager collectionManager = event.getCapacityUnit().elementCollectionManager;
        SSHPCounterController sshpcController = sshpccManager.getSSHPCC(collectionManager.getSegmentController());

        float capacity = sshpcController.getSystemCapacity(SSHPids.SHIELD_CAP);

        event.subtractShields(Math.round(event.getCapacity() * (1-capacity)));

        Main.print(0, "Shield capacity adjusted by " + capacity);
    }

    public static void handleShieldReg(SSHPCCManager sshpccManager, ShieldRegenCalculateEvent event) {
        SSHPCounterController sshpcController = sshpccManager.getSSHPCC(event.segmentController);

        float capacity = sshpcController.getSystemCapacity(SSHPids.SHIELD_REG);
        event.shieldRegen *= capacity;

        Main.print(0, "Shield regen adjusted by " + capacity);
    }

    public static void forceUpdateAllECMs(ManagedUsableSegmentController<?> ent, boolean twoSided) {

        if (twoSided) {
            Sendable[] sendables = SegmentControllerUtils.getDualSidedSendable(ent);
            for (Sendable sendable : sendables)
                if (sendable instanceof ManagedUsableSegmentController)
                    forceUpdateAllECMs((ManagedUsableSegmentController<?>) sendable, false);

            return;
        }

        for (ElementCollectionManager<?,?,?> manager : SSHPUtils.getAllCollectionManagers(ent))
            try {

                Method method = null;
                for (Class<?> c = manager.getClass(); ElementCollectionManager.class.isAssignableFrom(c); c = c.getSuperclass()) {
                    try {
                        method = c.getDeclaredMethod("onChangedCollection");
                        break;
                    } catch (NoSuchMethodException ignored) {}
                }
                if (method == null)
                    throw new NoSuchMethodException("Traversal failed to identify 'onChangedCollection' in "+ manager.getClass().getName());

                method.setAccessible(true);
                method.invoke(manager);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                Main.print(3,"Reflection failed, could not force collection update of " + manager.getName());
                e.printStackTrace();
            }
    }

    //Slightly modified variant of .existsEntity() by Schema
    public static boolean existsEntity(String ID, GameServerState serverState) {
        String nameOnly = ID.substring(1+ID.indexOf("_", 1+ID.indexOf("_")));
        Object2ObjectOpenHashMap<String, Sendable> objects =
                serverState.getLocalAndRemoteObjectContainer().getUidObjectMap();

        if(objects.containsKey(ID)){
            return true;
        }
        for(String uid : objects.keySet()){
            if(uid.toLowerCase(Locale.ENGLISH).equals((ID).toLowerCase(Locale.ENGLISH))){
                return true;
            }
        }
        try {
            return serverState.getDatabaseIndex().getTableManager().getEntityTable().getByUIDExact(nameOnly, 1).size() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
