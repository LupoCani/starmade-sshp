package org.lupocani.starmade_mod.sshp.utils;

public class SSHPids {
    static public final short THRUST = 8;
    static public final short AMC = 16;
    static public final short BEAM = 415;
    static public final short MISSILE = 32;

    static public final short SHIELD_REG = 478;
    static public final short SHIELD_CAP = 3;
}
