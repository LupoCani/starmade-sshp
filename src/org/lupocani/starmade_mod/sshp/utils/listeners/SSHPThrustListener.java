package org.lupocani.starmade_mod.sshp.utils.listeners;

import api.listener.fastevents.ThrusterElementManagerListener;
import org.lupocani.starmade_mod.sshp.SSHPCCManager;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;

import javax.vecmath.Vector3f;

public class SSHPThrustListener implements ThrusterElementManagerListener {

    private final SSHPCCManager sshpccManager;
    public SSHPThrustListener(SSHPCCManager sshpccManager){
        this.sshpccManager = sshpccManager;
    };

    @Override
    public void instantiate(ThrusterElementManager thrusterElementManager) {

    }

    @Override
    public float getSingleThrust(ThrusterElementManager thrusterElementManager, float v) {
        SSHPCounterController controller = sshpccManager.getSSHPCC(thrusterElementManager.getSegmentController());
        if (controller != null)
            return v * controller.getSystemCapacity(thrusterElementManager);

        return v;
    }

    @Override
    public float getSharedThrust(ThrusterElementManager thrusterElementManager, float v) {
        SSHPCounterController controller = sshpccManager.getSSHPCC(thrusterElementManager.getSegmentController());
        if (controller != null)
            return v * controller.getSystemCapacity(thrusterElementManager);

        return v;
    }

    @Override
    public float getThrustMassRatio(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public float getMaxSpeed(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public float getMaxSpeedAbsolute(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public Vector3f getOrientationPower(ThrusterElementManager thrusterElementManager, Vector3f vector3f) {
        return vector3f;
    }

    @Override
    public void handle(ThrusterElementManager thrusterElementManager) {

    }

    @Override
    public double getPowerConsumptionResting(ThrusterElementManager thrusterElementManager, double v) {
        return v;
    }

    @Override
    public double getPowerConsumptionCharging(ThrusterElementManager thrusterElementManager, double v) {
        return v;
    }
}
