package org.lupocani.starmade_mod.sshp.utils.listeners;

import api.listener.fastevents.HealingBeamHitListener;
import api.listener.fastevents.RepairBeamHitListener;
import api.listener.fastevents.segmentpiece.SegmentPieceKilledListener;
import api.listener.fastevents.segmentpiece.SegmentPieceAddListener;
import api.listener.fastevents.segmentpiece.SegmentPieceRemoveListener;

public abstract class SSHPSegmentPieceListener implements SegmentPieceAddListener, SegmentPieceRemoveListener, SegmentPieceKilledListener, RepairBeamHitListener, HealingBeamHitListener {
    //Implemented in main
}
