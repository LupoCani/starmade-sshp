package org.lupocani.starmade_mod.sshp.utils;

import org.lupocani.starmade_mod.sshp.SSHPCCManager;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;

public class SSHPDebug {
    public static void printControllers(SSHPCCManager manager) {
        manager.printSSHPList();
    }
}
