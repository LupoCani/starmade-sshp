package org.lupocani.starmade_mod.sshp.network;

import org.schema.game.common.data.player.PlayerState;

public interface SSHPSendable {
    void sendNetworkUpdate();

    void sendNetworkUpdatePrivate(PlayerState player);

    void scheduleNetworkUpdate();
}
