package org.lupocani.starmade_mod.sshp.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.lupocani.starmade_mod.sshp.Main;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class SSHPCCPacket extends Packet {
    private int entityId;
    private boolean rebooting;
    private boolean rebootFailed;
    private double rebootTime;

    public SSHPCCPacket(){};

    public SSHPCCPacket(int entityId, double rebootTime, boolean rebooting, boolean rebootFailed) {
        this.entityId = entityId;
        this.rebootTime = rebootTime;
        this.rebooting = rebooting;
        this.rebootFailed = rebootFailed;

    }

    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        entityId = packetReadBuffer.readInt();
        rebootTime = packetReadBuffer.readDouble();
        rebooting = packetReadBuffer.readBoolean();
        rebootFailed = packetReadBuffer.readBoolean();
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {
        packetWriteBuffer.writeInt(entityId);
        packetWriteBuffer.writeDouble(rebootTime);
        packetWriteBuffer.writeBoolean(rebooting);
        packetWriteBuffer.writeBoolean(rebootFailed);
    }

    @Override
    public void processPacketOnClient() {
        Main.print(0, "Client got SSHP reboot packet. ("+rebootTime+"s)");
        Main.sshpccManager.rebootFromNetwork(entityId, rebootTime, rebooting, rebootFailed);

    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        Main.print(3, "Server got SSHP reboot packet (should not happen).");
    }
}
