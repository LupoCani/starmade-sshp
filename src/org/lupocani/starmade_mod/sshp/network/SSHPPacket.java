package org.lupocani.starmade_mod.sshp.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.lupocani.starmade_mod.sshp.Main;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class SSHPPacket extends Packet {
    private int entityId;
    private short elementId;
    private float sshp;
    private float excess;
    private boolean isDamaged;

    public SSHPPacket(){};

    public SSHPPacket(int entityId, short elementId, float sshp, float excess, boolean isDamaged) {
        this.entityId = entityId;
        this.elementId = elementId;
        this.sshp = sshp;
        this.excess = excess;
        this.isDamaged = isDamaged;
    }

    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        entityId = packetReadBuffer.readInt();
        elementId = packetReadBuffer.readShort();
        sshp = packetReadBuffer.readFloat();
        excess = packetReadBuffer.readFloat();
        isDamaged = packetReadBuffer.readBoolean();
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {
        packetWriteBuffer.writeInt(entityId);
        packetWriteBuffer.writeShort(elementId);
        packetWriteBuffer.writeFloat(sshp);
        packetWriteBuffer.writeFloat(excess);
        packetWriteBuffer.writeBoolean(isDamaged);
    }

    @Override
    public void processPacketOnClient() {
        if (!Main.sshpccManager.updateFromNetwork(entityId, elementId, sshp, excess, isDamaged))
            Main.print(3, "SSHP network update failed.");
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        Main.print(3, "Server got SSHP update packet. Should not happen.");
    }
}
