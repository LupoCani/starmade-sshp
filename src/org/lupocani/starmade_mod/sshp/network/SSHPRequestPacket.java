package org.lupocani.starmade_mod.sshp.network;

import api.ModPlayground;
import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class SSHPRequestPacket extends Packet {
    private int entityId;

    public SSHPRequestPacket(){};

    public SSHPRequestPacket(int id) {
        entityId = id;
    }

    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        entityId = packetReadBuffer.readInt();
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {
        packetWriteBuffer.writeInt(entityId);
    }

    @Override
    public void processPacketOnClient() {
        Main.print(3, "Client got SSHP request packet. Should not happen.");
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        Main.print(0, "Server got SSHP request packet from "+playerState.getName()+". Responding.");
        SSHPCounterController controller = Main.sshpccManager.getSSHPCC(entityId);
        if (controller != null)
            controller.sendFullNetworkUpdatePrivate(playerState);
    }
}
