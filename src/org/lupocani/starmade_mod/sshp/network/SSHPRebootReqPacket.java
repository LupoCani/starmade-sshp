package org.lupocani.starmade_mod.sshp.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class SSHPRebootReqPacket extends Packet {
    private int entityId;

    public SSHPRebootReqPacket(){};

    public SSHPRebootReqPacket(int entityId) {
        this.entityId = entityId;
    }

    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        entityId = packetReadBuffer.readInt();
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {
        packetWriteBuffer.writeInt(entityId);
    }

    @Override
    public void processPacketOnClient() {
        Main.print(3, "Client got reboot request packet (should not happen).");
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        SegmentController controller = SSHPUtils.getPilotedEntityFromPlayer(playerState);
        if (controller == null || controller.getId() != entityId) return;

        Main.sshpccManager.rebootFromInput(entityId);
        Main.print(0, "Server got reboot request packet.");
    }
}
