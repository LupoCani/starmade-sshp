package org.lupocani.starmade_mod.sshp;

import api.common.GameClient;
import api.listener.Listener;
import api.listener.events.Event;
import api.listener.events.block.SegmentPieceAddEvent;
import api.listener.events.block.SegmentPieceRemoveEvent;
import api.listener.events.block.SegmentPieceSalvageEvent;
import api.listener.events.calculate.ShieldCapacityCalculateEvent;
import api.listener.events.calculate.ShieldRegenCalculateEvent;
import api.listener.events.controller.ClientInitializeEvent;
import api.listener.events.controller.ServerInitializeEvent;
import api.listener.events.entity.SegmentControllerFullyLoadedEvent;
import api.listener.events.gui.GUITopBarCreateEvent;
import api.listener.events.network.ClientSendableRemoveEvent;
import api.listener.events.player.PlayerChatEvent;
import api.listener.events.systems.ThrustCalculateEvent;
import api.listener.events.weapon.AnyWeaponDamageCalculateEvent;
import api.listener.events.weapon.UnitModifierHandledEvent;
import api.listener.events.block.SegmentPieceKillEvent;
//import api.listener.events.world.WorldSaveEvent;
import api.listener.events.world.WorldSaveEvent;
import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.HealingBeamHitListener;
import api.listener.fastevents.RepairBeamHitListener;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.PersistentObjectUtil;
import api.network.packets.PacketUtil;
import api.utils.StarRunnable;
import api.utils.gui.ModGUIHandler;
import org.lupocani.starmade_mod.sshp.UI.SSHPGUIControlManager;
import org.lupocani.starmade_mod.sshp.configs.SSHPConfig;
import org.lupocani.starmade_mod.sshp.network.*;
import org.lupocani.starmade_mod.sshp.utils.SSHPDebug;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.lupocani.starmade_mod.sshp.utils.listeners.SSHPSegmentPieceListener;
import org.lupocani.starmade_mod.sshp.utils.listeners.SSHPThrustListener;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.newgui.GUITopBar;
import org.schema.game.common.controller.BeamHandlerContainer;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.beam.repair.RepairBeamHandler;
import org.schema.game.common.data.SegmentPiece;
import api.listener.events.world.ServerSendableRemoveEvent;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Segment;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationHighlightCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.input.InputState;

import javax.annotation.Nullable;
import javax.vecmath.Vector3f;
import java.util.*;


/**
 * Main class for SSHP.
 * @author lupoCani
 */
public class Main extends StarMod {
    private static boolean disabled = false;
    private static boolean isServer = false;
    private static boolean isClient = false;
    final private static int error_level = 2;
    //0 = common, 1 = rare, 2 = once-per-session, 3 = error, 10 = manually triggered

    static public StarMod instance;
    static public SSHPCCManager sshpccManager;
    static public SSHPGUIControlManager controlManager;
    static public SSHPConfig config;
    final static private Stack<SSHPSendable> networkSchedule = new Stack<>();
    final static private Set<SegmentController> changedEntities = new HashSet<>();

    public static void main(String[] args) {
    }



    @Override
    public void onEnable() {
        Main.print(2,"SSHP Starting.");
        instance = this;
        config = new SSHPConfig(this);

        PacketUtil.registerPacket(SSHPPacket.class);
        PacketUtil.registerPacket(SSHPRequestPacket.class);
        PacketUtil.registerPacket(SSHPCCPacket.class);
        PacketUtil.registerPacket(SSHPRebootReqPacket.class);

        ArrayList<Object> listWithManager = PersistentObjectUtil.getObjects(getSkeleton(), SSHPCCManager.class);
        if (listWithManager.isEmpty()) {
            Main.print(2,"No saved data available.");
            sshpccManager = new SSHPCCManager();
            PersistentObjectUtil.addObject(getSkeleton(), sshpccManager);
        } else {
            Main.print(2,"Opening saved data.");
            sshpccManager = (SSHPCCManager) listWithManager.get(0);
            sshpccManager.postLoad();
        }

        /*
        Whenever a block is destoyed, identify which system (if any) it was a part of and adjust its SSHP.
         */

        SSHPSegmentPieceListener listener = new SSHPSegmentPieceListener() {
            //Whenever a block is destoyed, identify which system (if any) it was a part of and adjust its SSHP.
            @Override
            public void onBlockKilled(SegmentPiece segmentPiece, SendableSegmentController controller, @Nullable Damager dmg, boolean isOnServer) {
                if (isServer && !isOnServer) //In singleplayer, only handle the server-side call.
                    return;
                blockDestroyed(segmentPiece);
                changedEntities.add(controller);
            }

            //When a block is placed/removed/salvaged, run a general update over that entity.
            //The operation is cheap, but nevertheless buffered via a set to avoid duplicate invocation in a single tick.
            @Override
            public void onAdd(SegmentController segmentController, short i, byte b, byte b1, byte b2, byte b3, Segment segment, boolean b4, long l, boolean b5) {
                changedEntities.add(segment.getSegmentController());
            }

            @Override
            public void onBlockRemove(short i, int i1, byte b, byte b1, byte b2, byte b3, Segment segment, boolean b4, boolean b5) {
                changedEntities.add(segment.getSegmentController());
            }

            //Handle SSHP restoration from repair/healing beams.
            @Override
            public boolean handle(BeamState beamState, int beamHits, BeamHandlerContainer<?> container,
                                  SegmentPiece hitPiece, Vector3f from, Vector3f to, Timer timer,
                                  Collection<Segment> collection) {
                blockRepaired(hitPiece, null);
                changedEntities.add(hitPiece.getSegmentController());
                return false;
            }

            @Override
            public void hitFromShip(RepairBeamHandler repairBeamHandler, BeamState hittingBeam, int i,
                                    BeamHandlerContainer<SegmentController> beamHandlerContainer,
                                    SegmentPiece hitPiece, Vector3f from, Vector3f to,
                                    Timer timer, Collection<Segment> collection) {
                blockRepaired(hitPiece, hittingBeam);
                changedEntities.add(hitPiece.getSegmentController());
            }
        };

        StarLoader.registerListener(SegmentPieceSalvageEvent.class, new Listener<SegmentPieceSalvageEvent>() {
            @Override
            public void onEvent(SegmentPieceSalvageEvent event) {
                changedEntities.add(event.getSegmentController());
            }
        }, this);

        FastListenerCommon.segmentPieceAddListeners.add(listener);
        FastListenerCommon.segmentPieceKilledListeners.add(listener);
        FastListenerCommon.segmentPieceRemoveListeners.add(listener);
        FastListenerCommon.repairBeamHitListeners.add(listener);
        FastListenerCommon.healingBeamHitListeners.add(listener);

        /* *
         * For SSHP to have any effect, the system estimating their own effectiveness must be intercepted so the nerf
         * from SSHP loss can be applied. This listener does so for thrusters.
         */

        FastListenerCommon.thrusterElementManagerListeners.add(new SSHPThrustListener(sshpccManager));

        StarLoader.registerListener(UnitModifierHandledEvent.class, new Listener<UnitModifierHandledEvent>() {
            @Override
            public void onEvent(UnitModifierHandledEvent event) {
                SSHPUtils.handleWeaponCombo(sshpccManager, event);
            }
        }, this);

        StarLoader.registerListener(AnyWeaponDamageCalculateEvent.class, new Listener<AnyWeaponDamageCalculateEvent>() {
            @Override
            public void onEvent(AnyWeaponDamageCalculateEvent event) {
                SSHPUtils.handleWeapon(sshpccManager, event);
            }
        }, this);

        StarLoader.registerListener(ShieldCapacityCalculateEvent.class, new Listener<ShieldCapacityCalculateEvent>() {
            @Override
            public void onEvent(ShieldCapacityCalculateEvent event) {
                SSHPUtils.handleShieldCap(sshpccManager, event);
            }
        }, this);

        StarLoader.registerListener(ShieldRegenCalculateEvent.class, new Listener<ShieldRegenCalculateEvent>() {
            @Override
            public void onEvent(ShieldRegenCalculateEvent event) {
                SSHPUtils.handleShieldReg(sshpccManager, event);
            }
        }, this);

        /* *
         * These methods handle the loading/unloading of segment controllers and their corresponding SSHPCCs,
         * as well as saving data to disk at runtime.
         */

        /*
        When a new segment controller is (fully) created, create a SSHP counter controller for it
        and populuate it with SSHP counters.
         */
        StarLoader.registerListener(SegmentControllerFullyLoadedEvent.class, new Listener<SegmentControllerFullyLoadedEvent>() {
            @Override
            public void onEvent(SegmentControllerFullyLoadedEvent event) {
                if (isServer && !event.isServer()) //In singleplayer, only handle the server-side call.
                    return;
                final SegmentController controller = event.getController();
                if (!(controller instanceof ManagedUsableSegmentController)) return;
                sshpccManager.buildSSHPCC((ManagedUsableSegmentController<?>) controller);

                if (!isServer)
                    requestUpdate(controller.getId());

                new StarRunnable() {
                    @Override
                    public void run() {
                        changedEntities.add(controller);
                    }
                }.runLater(instance, 25);
            }
        }, this);

        /*
        When a segment controller is unloaded, move the corresponding SSHPCC into cold storage. (server side)
         */

        StarLoader.registerListener(ServerSendableRemoveEvent.class, new Listener<ServerSendableRemoveEvent>() {
            @Override
            public void onEvent(ServerSendableRemoveEvent event) {
                if (!(event.getSendable() instanceof SegmentController)) return;
                if (event.getCondition() != Event.Condition.POST) return;
                sshpccManager.unloadSSHPCC((SegmentController) event.getSendable(), true);
            }
        }, this);

        /*
        When a segment controller is unloaded, discard the corresponding SSHPCC. (client side)
         */
        StarLoader.registerListener(ClientSendableRemoveEvent.class, new Listener<ClientSendableRemoveEvent>() {
            @Override
            public void onEvent(ClientSendableRemoveEvent event) {
                if (isServer) return;
                if (!(event.getSendable() instanceof SegmentController)) return;
                if (event.getCondition() != Event.Condition.POST) return;
                sshpccManager.unloadSSHPCC((SegmentController) event.getSendable(), false);
            }
        }, this);

        /*
        Save SSHP data whenever there's an autosave.
        Doesn't cover game shutdown saves for some reason, see .onDisable() for handling that case.
         */
        //*
        StarLoader.registerListener(WorldSaveEvent.class, new Listener<WorldSaveEvent>() {
            @Override
            public void onEvent(WorldSaveEvent event) {
                if (event.condition == Event.Condition.PRE)
                    saveData(false);
            }
        }, this);//*/

        StarLoader.registerListener(GUITopBarCreateEvent.class, new Listener<GUITopBarCreateEvent>() {
            @Override
            public void onEvent(GUITopBarCreateEvent event) {

                Main.print(0,"Connecting GUI top bar button.");

                GUITopBar.ExpandedButton button = event.getDropdownButtons().get(0);

                Main.print(0,"Got button " + button.toString());
                button.addExpandedButton("SUBSYSTEM HP", new GUICallback() {
                    @Override
                    public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                        if (!mouseEvent.pressedLeftMouse())
                            return;

                        Main.print(0,"Opening GUI.");
                        if (controlManager != null) {
                            GameClient.getClientState().getGlobalGameControlManager().getIngameControlManager()
                                    .getPlayerGameControlManager().deactivateAll();
                            controlManager.setActive(true);
                        }
                        Main.print(0,"GUI opened.");
                    }

                    @Override
                    public boolean isOccluded() {
                        return false;
                    }
                }, new GUIActivationHighlightCallback() {
                    @Override
                    public boolean isHighlighted(InputState inputState) {
                        return false;
                    }

                    @Override
                    public boolean isVisible(InputState inputState) {
                        return true;
                    }

                    @Override
                    public boolean isActive(InputState inputState) {
                        return SSHPUtils.getPilotedEntityFromPlayer(GameClientState.instance.getPlayer()) != null;
                    }
                });
                Main.print(0,"Button added.");
            }
        }, this);

        new StarRunnable() {
            @Override
            public void run() {
                sendNetworkUpdate();
            }
        }.runTimer(this, 50);

        StarLoader.registerListener(PlayerChatEvent.class, new Listener<PlayerChatEvent>() {
            @Override
            public void onEvent(PlayerChatEvent event) {
                if (event.getText().equals("listsshp")) {
                    SSHPDebug.printControllers(sshpccManager);
                }
            }
        }, this);

        new StarRunnable() {
            @Override
            public void run() {
                handleBlockAlterations();
                handleRebootTick();
            }
        }.runTimer(this, 1);
    }

    @Override
    public void onDisable() {
        if (isServer)
            saveData(true);
        disabled = true;
    }

    public static void scheduleNetworkUpdate(SSHPSendable sshpSendable) {
        networkSchedule.push(sshpSendable);
    }

    private void sendNetworkUpdate() {
        while (!networkSchedule.isEmpty())
            networkSchedule.pop().sendNetworkUpdate();
    }

    private void requestUpdate(int entityID) {
        SSHPRequestPacket packet = new SSHPRequestPacket(entityID);
        PacketUtil.sendPacketToServer(packet);
    }

    private static void handleRebootTick() {
        sshpccManager.handleShipReboots();
    }

    private void handleBlockAlterations() {
        for (SegmentController controller : changedEntities)
            blockAlteration(controller);

        changedEntities.clear();
    }

    public static void handleGUIReboot(PlayerState playerState) {
        SegmentController entity = SSHPUtils.getPilotedEntityFromPlayer(playerState);
        if (entity == null) return;
        if (!entity.isOnServer()) //Disabled
            sshpccManager.rebootFromInput(entity.getId());
        PacketUtil.sendPacketToServer(new SSHPRebootReqPacket(entity.getId()));
    }

    public static SSHPCounterController getClientPilotedSSHPCC() {
        SegmentController controller = SSHPUtils.getPilotedEntityFromPlayer(GameClientState.instance.getPlayer());
        if (controller == null) return null;

        return Main.sshpccManager.getSSHPCC(controller.getId());
    }

    public static SSHPCounterController getClientSelectedSSHPCC() {
        PlayerState playerState = GameClientState.instance.getPlayer();
        return Main.sshpccManager.getSSHPCC(playerState.getSelectedEntityId());
    }

    /**
     * Invoke for block destruction.
     * @param segmentPiece The segment piece destroyed.
     */
    private void blockDestroyed(SegmentPiece segmentPiece) {
        if (segmentPiece.getType() == 0)
            return;
        try {
            sshpccManager.onBlockKill(segmentPiece);
        } catch (Exception e) {
            Main.print(3, "Block destroy exception.");
            Main.print(3, e.toString());
            e.printStackTrace();
        }
    }

    private void blockRepaired(SegmentPiece piece, BeamState beam) {
        try {
            if (beam == null)
                sshpccManager.onBlockRepair(piece, 1);
            else
                sshpccManager.onBlockRepair(piece, beam.getPower()/piece.getInfo().getMaxHitPointsFull());
        } catch (Exception e) {
            Main.print(3, "Block repair exception.");
            Main.print(3, e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Invoke for all nondestructive block changes, such as placement, removal and salvage.
     * @param sc The segment controller affected.
     */
    private void blockAlteration(SegmentController sc) {
        try {
            sshpccManager.onBlockChange(sc);
        } catch (Exception e) {
            Main.print(3, "Block place/remove/salvage exception.");
            Main.print(3, e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Saves SSHP data to disk using the JSON-based persistent object utility.
     * @param shutdown Whether or not the saving is due to server shutdown.
     */
    private void saveData(boolean shutdown) {
        if (disabled)
            return;

        if (shutdown) {
            sshpccManager.unloadAll();
        } else {
            sshpccManager.generalUpdate();
            sshpccManager.prepSave();
        }
        PersistentObjectUtil.save(getSkeleton());
        if (shutdown) {
            Main.print(2, "Saved data.");
        } else {
            sshpccManager.postSave();
            Main.print(1, "General update and save performed.");
        }
    }

    public static void print(int lvl, String string) {
        if (lvl < error_level) return;
        System.err.println(String.format(" [sshp](%s|%s) %s", (isServer ? "s":"-"), (isClient ? "c":"-"), string));
    }

    @Override
    public void onServerCreated(ServerInitializeEvent event) {
        super.onServerCreated(event);
        isServer = true;

        Main.print(2, "Verifying unloaded ships still exist in database.");
        sshpccManager.checkDataIntegrity(event.getServerState());
        Main.print(2, "Verification done.");
    }

    @Override
    public void onClientCreated(ClientInitializeEvent event) {
        super.onClientCreated(event);
        isClient = true;

        controlManager = new SSHPGUIControlManager(GameClient.getClientState());
        ModGUIHandler.registerNewControlManager(this.getSkeleton(), controlManager);
    }
}
