package org.lupocani.starmade_mod.sshp.configs;

import api.mod.StarMod;
import api.utils.simpleconfig.*;

public class SSHPConfig extends SimpleConfigContainer {
    public SSHPConfig(StarMod mod) {
        super(mod, "sshp_config", false);
    }

    public final SimpleConfigDouble effSizeExponent =
            new SimpleConfigDoubleClamp(this,
                    "effSizeExponent",
                    0.85, 0.1, 1.0,
                    "The exponent by which effective size grows.");
    public final SimpleConfigDouble disintegrityExponent =
            new SimpleConfigDoubleClamp(this,
                    "disintegrityExponent",
                    0.5, 0.0, 1.0,
                    "The exponent by which the integrity penalty grows.");
    public final SimpleConfigDouble sizeRebate =
            new SimpleConfigDoubleClamp(this,
                    "sizeRebate",
                    5.0, 0.0, null,
                    "A rebate imposed before calculating effective size.");
    public final SimpleConfigDouble sizeFactorStart =
            new SimpleConfigDoubleClamp(this,
                    "sizeFactorStart",
                    0.6,0.0, 1.0,
                    "The starting value of the effective size.");
    public final SimpleConfigDouble sizeFactorEnd =
            new SimpleConfigDoubleClamp(this,
                    "sizeFactorEnd",
                    0.25,0.0, 1.0,
                    "The minimum value of the effective size (before integrity penalty).");
    public final SimpleConfigDouble rebootBaseTime =
            new SimpleConfigDoubleClamp(this,
                    "rebootBaseTime",
                    20.0, 0.0, null,
                    "The time needed to reboot a ship.");
    public final SimpleConfigDouble overheatPercentage =
            new SimpleConfigDoubleClamp(this,
                    "overheatPercentage",
                    30.0, 0.0, 100.0,
                    "The collective SSHP threshold before reactor overheat.");

    public final SimpleConfigDouble excessDamage =
            new SimpleConfigDoubleClamp(this,
                    "excessDamage",
                    0.0, 0.0, null,
                    "Scales the impact of further damage to a system with zero SSHP on shipwide SHP.");

    public final SimpleConfigBool flatExcessDamage =
            new SimpleConfigBool(this,
                    "flatExcessDamage",
                    true,
                    "Whether excess damage should disregard the dynamic damage factor.");
}

class SimpleConfigDoubleClamp extends SimpleConfigDouble {
    Double min; Double max;
    SimpleConfigDoubleClamp(SimpleConfigContainer container, String name, Double value, Double min, Double max, String comment) {
        super(container, name, value, comment);
        this.min = min;
        this.max = max;
    }
    
    @Override
    protected boolean validateValue() {
        double val = value;
        if (min != null)
            value = Math.max(min, value);
        if (max != null)
            value = Math.min(max, value);
        return value != val;
    }

    @Override
    public String getComment() {
        if (commentStr == null)
            return null;

        return String.format("%s %s (%s < %f < %s): %s", typeName, name,
                (min == null || Double.isNaN(min) ? "n/a" : min),
                defaultValue,
                (max == null || Double.isNaN(max) ? "n/a" : max),
                commentStr);
    }
}
