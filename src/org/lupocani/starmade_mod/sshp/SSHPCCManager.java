package org.lupocani.starmade_mod.sshp;

import api.ModPlayground;
import api.common.GameCommon;
import org.lupocani.starmade_mod.sshp.persist.SerialSSHPCC;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import java.util.*;

/**
 * Central class for tracking SSHP state. Contains a list of loaded ships (SSHP counter controllers), a list of
 * unloaded ships, and a secondary list of (unloaded deep copies of) loaded SSHPCCs populated only when the class is
 * being serialized.
 * @author lupoCani
 * */

public class SSHPCCManager {
    transient private HashMap<String, SSHPCounterController> listOfShips;
    final private HashMap<String, SerialSSHPCC> listOfShips_unloaded;
    final private ArrayList<SerialSSHPCC> listOfShips_temp;

    final private Set<String> shipsRebooting;

    public SSHPCCManager(){
        listOfShips =           new HashMap<>();
        listOfShips_unloaded =  new HashMap<>();
        listOfShips_temp =      new ArrayList<>();

        shipsRebooting = new HashSet<>();
    }

    public SSHPCounterController getSSHPCC(SegmentController segmentController) {
        if (!(segmentController instanceof ManagedUsableSegmentController<?>)) return null;

        String id = segmentController.getUniqueIdentifier();

        //Check for the SSHPCC by ID, if that fails try to load it by ID.
        if (listOfShips.containsKey(id) || loadSSHPCC(segmentController))
            return listOfShips.get(id);

        //If finding and loading both fail, make a new one.
        Main.print(0,"Ship ("+id+") not found, creating.");
        return this.setupSSHPCC((ManagedUsableSegmentController<?>) segmentController);

    }

    public boolean loadSSHPCC(SegmentController segmentController) {
        if (!(segmentController instanceof ManagedUsableSegmentController)) return false;
        String id = segmentController.getUniqueIdentifier();

        SerialSSHPCC unloaded = listOfShips_unloaded.remove(id);
        if (unloaded == null) {Main.print(0,"Failed to load ship. " + id); return false;}

        listOfShips.put(id, new SSHPCounterController(unloaded, (ManagedUsableSegmentController<?>) segmentController));

        Main.print(0,"Ship loaded.");
        return true;
    }

    public boolean unloadSSHPCC(SegmentController segmentController, boolean keep) {
        return unloadSSHPCC_id(segmentController.getUniqueIdentifier(), keep);
    }

    public boolean unloadSSHPCC_id(String id, boolean keep) {
        SSHPCounterController controller = listOfShips.remove(id);
        if (controller == null) return false;

        if (keep && !controller.segmentController.isMarkedForPermanentDelete()) {
            listOfShips_unloaded.put(id, controller.getSerial());
            Main.print(0,"Unloaded entity <" + id + ">");
        }
        else
            Main.print(0,"Discarded entity <" + id + "> " + (keep ? "(entity destroyed)" : "(is client)"));
        return true;
    }

    public void unloadAll() {
        Set<String> shipIDs = new HashSet<>(listOfShips.keySet());
        for (String id : shipIDs)
            unloadSSHPCC_id(id, true);
    }

    /**
     * Populate the temporary ship list with all the loaded ships, before JSON serialization is applied to the class,
     * which won't include the transient main list.
     */
    public void prepSave() {
        for (SSHPCounterController controller: listOfShips.values()) {
            listOfShips_temp.add(controller.getSerial());
        }
    }

    /**
     * De-populate the temporary list again.
     */
    public void postSave() {
        listOfShips_temp.clear();
    }

    /**
     * Once the class is de-serialized, move all the ships back from the temporary list to the (transient, thus empty)
     * main list.
     */
    public void postLoad() {
        listOfShips = new HashMap<>();
        for (SerialSSHPCC unloaded: listOfShips_temp){
            listOfShips_unloaded.put(unloaded.segmentControllerID, unloaded);
        }
        for (String key: listOfShips_unloaded.keySet()){
            Main.print(0,key+ " : "+listOfShips_unloaded.get(key).segmentControllerID);
        }
    }

    /**
     * Verifies that the database doesn't contain mis-indexed SSHPCCs, or SSHPCCs for ships that no longer exist.
     * @param serverState Non-null server state object to verify against.
     */
    public void checkDataIntegrity(GameServerState serverState) {
        Set<SerialSSHPCC> corruptControllers = new HashSet<>();

        for (SerialSSHPCC controller : listOfShips_unloaded.values()) {
            String ID = controller.segmentControllerID;
            //Check that the ID isn't somehow corrupted.
            if (listOfShips_unloaded.get(ID) != controller) {
                corruptControllers.add(controller);
                Main.print(3,"Entity ID corrupt: " + ID);
            }
            //Check that the entity still exists in cold storage.
            if (!SSHPUtils.existsEntity(ID, serverState)) {
                corruptControllers.add(controller);
                Main.print(3,"Entity gone from cold storage: " + ID);
            }
        }
        //Remove any entities that failed verification.
        for (SerialSSHPCC controller : corruptControllers) {
            listOfShips_unloaded.remove(controller.segmentControllerID);
        }

    }

    /**
     * Run a general update on all ships.
     */
    public void generalUpdate() {
        for (SSHPCounterController controller : listOfShips.values()) {
            controller.generalUpdate();
        }
    }

    public void handleShipReboots() {
        Set<String> shipsDone = new HashSet<>();
        for (String id : shipsRebooting) {
            SSHPCounterController controller = listOfShips.get(id);

            if (controller == null || controller.tickReboot(0.05))
                shipsDone.add(id);
        }
        shipsRebooting.removeAll(shipsDone);
    }

    /**
     * Tell an SSHPCC a block has been destroyed.
     * @param segmentPiece The block destroyed. Includes information about which entity is affected.
     */
    public void onBlockKill(SegmentPiece segmentPiece) {
        SSHPCounterController sshpCounterController = getSSHPCC(segmentPiece.getSegmentController());

        if (sshpCounterController != null)
            sshpCounterController.onBlockKill(segmentPiece);
    }

    public void onBlockRepair(SegmentPiece segmentPiece, float power) {
        SSHPCounterController sshpCounterController = getSSHPCC(segmentPiece.getSegmentController());

        if (sshpCounterController != null)
            sshpCounterController.onBlockRepair(segmentPiece, power);
    }

    /**
     * Tell an SSHPCC a block has been added. Since the response is simply to re-check the SSHPC size, which is nearly
     * as cheap as checking which SSHPC is affected, simply re-check all SSHPCs on the SSHPCC.
     * @param segmentController The segment controller affected.
     */
    public void onBlockChange(SegmentController segmentController) {
        SSHPCounterController sshpCounterController = getSSHPCC(segmentController);

        if (sshpCounterController != null)
            sshpCounterController.generalUpdate();
    }

    private SSHPCounterController setupSSHPCC(ManagedUsableSegmentController<?> musController) {

        SSHPCounterController sshpCounterController = new SSHPCounterController(musController);
        this.listOfShips.put(musController.getUniqueIdentifier(), sshpCounterController);

        Main.print(0,"Ship created. Number of ships is " + listOfShips.size());

        return sshpCounterController;
    }

    /**
     * Invoke for entity creation.
     * Ensures the creation of a SSHP counter for each system individually.
     * The sshpccManager handles creating/loading the SSHP counter controller where necessary,
     * as well as creating/loading the SSHP counter.
     * @param controller The newly loaded segment controller.
     */
    public void buildSSHPCC(ManagedUsableSegmentController<?> controller) {
        ArrayList<UsableElementManager<?, ?, ?>> elementManagers = SSHPUtils.getAllElementManagers(controller);

        Main.print(0,"Ship (ID "+ controller.getId() +") has " + elementManagers.size() + " UEMs.");

        for (UsableElementManager<?, ?, ?> elementManager : elementManagers) {
            try {
                if (setupSSHPC(elementManager))
                    Main.print(0,"System created.");
            } catch (Exception e) {
                Main.print(3,"New system exception.");
                ModPlayground.broadcastMessage(e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets up the SSHP counter for a new system. Handles creating/loading the SSHP counter controller if neccessary,
     * as well as creating/loading from memory the SSHPC assuming it doesn't/does exist.
     * @param elementManager The new system.
     * @return The SSHPC loaded/created.
     */
    public boolean setupSSHPC(UsableElementManager<?,?,?> elementManager) {
        //Get the MUSC.
        SegmentController segmentController = elementManager.getSegmentController();

        //Fetch the MUSC's SSHPCC. This method will create one for the MUSC if one does not yet exist.
        SSHPCounterController sshpCounterController = getSSHPCC(segmentController);

        //Tell the SSHPCC to create a new SSHPC for the system created.
        return sshpCounterController.newSSHPCounter(elementManager);
    }

    /**
     * Receive a network update packet.
     * @param entityID The packet entity ID.
     * @param elementID The packet block ID of the system affected.
     * @param sshp The SSHP of the system affected.
     * @return The network update success status.
     */
    public boolean updateFromNetwork(int entityID, short elementID, float sshp, float excess, boolean isDamaged) {
        SSHPCounterController controller = getSSHPCC(entityID);
        if (controller == null) return false;

        return controller.updateCounterFromNetwork(elementID, sshp, excess, isDamaged);
    }

    public boolean rebootFromNetwork(int entityID, double time, boolean rebooting, boolean failed) {
        SSHPCounterController controller = getSSHPCC(entityID);
        if (controller == null) return false;

        Main.print(0,"Server Found entity.");

        controller.updateFromNetwork(time, rebooting, failed);
        if (rebooting)
            shipsRebooting.add(controller.segmentController.getUniqueIdentifier());
        return true;
    }

    public boolean rebootFromInput(int entityID) {
        SSHPCounterController controller = getSSHPCC(entityID);
        if (controller == null) return false;

        Main.print(0,"Server Found entity. Reboot time is "+Main.config.rebootBaseTime.value);

        controller.setReboot(Main.config.rebootBaseTime.value);
        shipsRebooting.add(controller.segmentController.getUniqueIdentifier());
        return true;
    }

    public SSHPCounterController getSSHPCC(int entityID) {
        Sendable object = GameCommon.getGameObject(entityID);
        if (!(object instanceof SegmentController)) {
            Main.print(1,"Warning, enity ID "+entityID+" not found client-side.");
            return null;
        }

        return getSSHPCC((SegmentController) object);
    }

    public void printSSHPList() {
        for (SSHPCounterController controller : listOfShips.values()) {
            Main.print(100, "(" + controller.segmentController.getId() + ") " + controller.segmentControllerID + ": " + controller.sshpsToString());
        }
    }
}
