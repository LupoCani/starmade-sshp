package org.lupocani.starmade_mod.sshp.api;

import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.UsableElementManager;

import java.util.ArrayList;

public class SSHPModInterface {
    final static public ArrayList<ModSystemIdCallback> modSystemIdCallbacks = new ArrayList<>();

    public static float getSSHPFraction(SegmentController controller, short id) {
        SSHPCounterController counterController = Main.sshpccManager.getSSHPCC(controller);
        if (counterController == null)
            return 1;

        return counterController.getSystemCapacity(id);
    }

    public static void registerSSHPSystem(ModSystemIdCallback callback) {
        modSystemIdCallbacks.add(callback);
    }

    public static short getModUEMId(UsableElementManager<?,?,?> UEM) {
        for (ModSystemIdCallback callback : modSystemIdCallbacks) {
            Short id = callback.getUEMblockId(UEM);
            if (id != null && id > 0)
                return id;
        }
        return -1;
    }
}
