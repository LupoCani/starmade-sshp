package org.lupocani.starmade_mod.sshp.api;

import org.schema.game.common.controller.elements.UsableElementManager;

public interface ModSystemIdCallback {
    Short getUEMblockId(UsableElementManager<?,?,?> manager);
}
