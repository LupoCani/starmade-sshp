package org.lupocani.starmade_mod.sshp.persist;

import api.mod.StarLoader;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

public class SerialSSHPC {

    public float subSystemHP;
    public float maxSSHP;
    public float damageFactor;
    public boolean isDamaged;


    public final short blockID;
    public final String systemName;

    public final String blockName; //Fallback persistent ID for mod blocks
    public float excessDamage;

    public SerialSSHPC(float sshp, float msshp, float exdmg,  float dfac, boolean dm, short id, String nm) {
        subSystemHP = sshp;
        maxSSHP = msshp;
        excessDamage = exdmg;
        damageFactor = dfac;
        isDamaged = dm;
        systemName = nm;

        if (ElementKeyMap.getInfo(id).isVanilla())
            blockID = id;
        else blockID = -1;

        blockName = ElementInformation.getKeyId(id);
    }

}
