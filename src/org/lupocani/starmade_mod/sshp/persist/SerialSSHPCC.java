package org.lupocani.starmade_mod.sshp.persist;

import org.lupocani.starmade_mod.sshp.SSHPCounter;

import java.util.ArrayList;
import java.util.Collection;

public class SerialSSHPCC {

    public final ArrayList<SerialSSHPC> sshpCounters;
    public final String segmentControllerID;

    public double rebootCountdown;
    public boolean rebooting;
    public boolean rebootFailed;

    public SerialSSHPCC(double cdown, boolean boot, boolean bootfail, String id, Collection<SSHPCounter> counters) {
        segmentControllerID = id;
        sshpCounters = new ArrayList<>();
        rebootCountdown = cdown;
        rebooting = boot;
        rebootFailed = bootfail;

        for (SSHPCounter counter : counters) {
            sshpCounters.add(counter.getSerial());
        }
    }
}
