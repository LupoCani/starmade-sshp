package org.lupocani.starmade_mod.sshp;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import org.lupocani.starmade_mod.sshp.network.SSHPCCPacket;
import org.lupocani.starmade_mod.sshp.network.SSHPSendable;
import org.lupocani.starmade_mod.sshp.persist.SerialSSHPC;
import org.lupocani.starmade_mod.sshp.persist.SerialSSHPCC;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentControllerHpController;
import org.schema.game.common.controller.SegmentControllerHpControllerInterface;
import org.schema.game.common.controller.elements.UsableElementManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorElementManager;
import org.schema.game.common.controller.elements.power.reactor.MainReactorUnit;
import org.schema.game.common.controller.elements.power.reactor.PowerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.PlayerState;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/** SSHP counter controller
 * Handles the SSHP counters on a given entity.
 * @author lupoCani
 */
public class SSHPCounterController implements SSHPSendable {
    protected final Map<Short, SSHPCounter> sshpCounters;
    public final String segmentControllerID;

    private double tsshp = 1;

    public double rebootCountdown;
    public boolean rebooting;
    public boolean rebootFailed;
    public final ManagedUsableSegmentController<?> segmentController;
    private boolean networkSemaphore;

    /**
     * Created a fresh SSHP counter controller, typically for a just-spawned entity.
     * @param musController The new MUSC object the SSHPCC follows.
     */
    public SSHPCounterController(ManagedUsableSegmentController<?> musController) {
        //super(musController.getUniqueIdentifier());
        sshpCounters = new HashMap<>();
        segmentControllerID = musController.getUniqueIdentifier();
        segmentController = musController;
        networkSemaphore = false;
    }

    /**
     * Loads a full SSHP counter controller from the unloaded (for serialization) version and a new MUSC object.
     * Tries to hook up every UEM in the MUSC with an existing controller.
     * @param serialSSHPCC The unloaded SSHPCC data.
     * @param musController The new MUSC object the SSHPCC follows.
     */
    public SSHPCounterController(SerialSSHPCC serialSSHPCC, ManagedUsableSegmentController<?> musController) {
        //super(controller.getUniqueIdentifier());
        sshpCounters = new HashMap<>();
        segmentControllerID = musController.getUniqueIdentifier();
        segmentController = musController;
        networkSemaphore = false;

        Main.print(0,"Loading SSHPCC.");
        Main.print(0,"Ship (loading) has " + SSHPUtils.getAllElementManagers(musController).size() + " UEMs.");

        List<SerialSSHPC> counters = new LinkedList<>(serialSSHPCC.sshpCounters);

        Map<Short, UsableElementManager<?,?,?>> uems = new HashMap<>();
        for (UsableElementManager<?,?,?> uem : SSHPUtils.getAllElementManagers(musController))
            uems.put(SSHPUtils.getUEMBlockID(uem), uem);

        for (SerialSSHPC unloaded : counters) {
            UsableElementManager<?,?,?> uem = uems.get(unloaded.blockID);
            if (uem == null)
                Main.print(3, "Missing UEM for saved SSHPC (id " + unloaded.blockID + ") " + unloaded.systemName);
            else {
                SSHPCounter counter = new SSHPCounter(unloaded, uem, this);
                if (counter.blockID > 0)
                    sshpCounters.put(counter.blockID, counter);
                else
                    Main.print(3, "No matching ID for modded block " + unloaded.blockName);
            }
        }
    }

    /**
     * Gets the SSHP counter that's responsible for a given system, assuming one exists, via the block ID.
     * The SSHPC returned for a given UEM does not necessarily have that UEM as a member, given that there are
     * two UEMs available for any one system in singleplayer.
     * @param elementManager The element manager for the system in question.
     * @return The SSHP counter for the system in question.
     */
    private SSHPCounter getSSSHPCounter(UsableElementManager<?,?,?> elementManager) {
        if (segmentControllerID == null) throw new NullPointerException("getSSSHPCounter called with null ID");
        if (elementManager == null) throw new NullPointerException("getSSSHPCounter called with null SC");
        if (elementManager.getSegmentController() == null) throw new NullPointerException("getSSSHPCounter called with null SC.");

        if (!elementManager.getSegmentController().getUniqueIdentifier().equals(segmentControllerID)) return null;

        short id = SSHPUtils.getUEMBlockID(elementManager);
        SSHPCounter sshpCounter = sshpCounters.get(id);
        if (sshpCounter != null && sshpCounter.blockID == id)
            return sshpCounter;

        return null;
    }

    public boolean newSSHPCounter(UsableElementManager<?,?,?> elementManager) {

        if (getSSSHPCounter(elementManager) != null) {
            Main.print(0,"EM already exists.");
            return true;
        }

        if (SSHPUtils.getUEMBlockID(elementManager) < 0) return false;

        Main.print(0,"New collection manager.");
        SSHPCounter counter = new SSHPCounter(elementManager, this);
        sshpCounters.put(counter.blockID, counter);
        Main.print(0,"Done.");
        return true;
    }

    public SSHPCounter getSSHPCounterFromPiece(SegmentPiece sPiece) {
        for (SSHPCounter sshpCounter : sshpCounters.values()) {
            if (sshpCounter.containsSegmentPiece(sPiece))
                return sshpCounter;
        }
        Main.print(0,"No match.");
        //segmentController.isFullyLoaded();
        return null;
    }

    public boolean onBlockKill(SegmentPiece sPiece) {
        killReboot();
        for (SSHPCounter sshpCounter : sshpCounters.values()) {
            if (sshpCounter.onBlockKill(sPiece)) {
                checkOverheat();
                return true;
            }
        }
        Main.print(0,"No SSHPC matches.");
        return false;
    }

    public void onBlockRepair(SegmentPiece sPiece, float power) {
        SSHPCounter counter = sshpCounters.get(sPiece.getType());
        if (counter != null) {
            counter.onBlockRepair(power);
            SSHPUtils.forceUpdateAllECMs(segmentController,true);
        }
    }

    private void updateTSSHP() {
        float msshpt = 0;
        float sshpt = 0;
        double min = Main.config.overheatPercentage.getValue()/100.0;
        for (SSHPCounter counter : sshpCounters.values()) {
            msshpt += counter.maxSSHP + counter.excessDamage;
            sshpt += counter.subSystemHP;
        }
        this.tsshp = sshpt/msshpt;
        Main.print(0,"Total SSHP is "+sshpt+"/"+msshpt+"="+(this.tsshp)+" (min is "+min+")");
    }

    private void checkOverheat() {
        if (sshpccHealth() > 0)
            return;

        SSHPUtils.forceOverheat(segmentController);
    }

    public double sshpccHealth() {
        if (isRebooting())
            return 0;
        double min = Main.config.overheatPercentage.getValue()/100.0;
        return (this.tsshp - min) / (1-min);
    }

    public void setReboot(double time) {
        Main.print(0,"Reboot set.");
        rebootCountdown = time;
        rebooting = true;
        rebootFailed = false;

        sendNetworkUpdate();
        SSHPUtils.forceUpdateAllECMs(segmentController,true);
    }

    public void killReboot() {
        rebootFailed = rebooting;
    }

    public boolean tickReboot(double tickLength) {
        rebootCountdown -= tickLength;
        if (rebootCountdown <= 0) {
            rebootCountdown = 0;
            Main.print(0,"Reboot done.");
            concludeReboot();
            if (rebootFailed)
                Main.print(0,"(reboot was not successful)");
            return true;
        }
        return false;
    }

    public void concludeReboot() {
        for (SSHPCounter sshpCounter : sshpCounters.values())
            sshpCounter.onShipReboot(rebootFailed);
        updateTSSHP();

        SSHPUtils.forceUpdateAllECMs(segmentController,true);
        Main.print(0,"Ship SSHP rebooted.");
    }

    public boolean isRebooting() {
        return rebootCountdown > 0;
    }

    public void generalUpdate() {
        for (SSHPCounter sshpCounter : sshpCounters.values()) {
            sshpCounter.generalUpdate();
        }
        updateTSSHP();
    }

    public float getSystemCapacity(UsableElementManager<?,?,?> elementManager) {
        SSHPCounter sshpCounter = getSSSHPCounter(elementManager);

        if (sshpCounter != null) {
            return sshpCounter.getFractSSHP();
        }
        return 1;
    }

    public float getSystemCapacity(short systemID) {
        SSHPCounter sshpCounter = sshpCounters.get(systemID);
        if (sshpCounter != null) {
            return sshpCounter.getFractSSHP();
        }
        Main.print(0,"Counter for "+systemID+" not found.");
        return 1;
    }

    public boolean updateCounterFromNetwork(short elementID, float sshp, float excess, boolean isDamaged) {
        for (SSHPCounter sshpCounter : sshpCounters.values()) {
            if (sshpCounter.blockID == elementID) {
                sshpCounter.updateFromNetwork(sshp, excess, isDamaged);
                return true;
            }
        }
        return false;
    }

    public void updateFromNetwork(double rebootCountdown, boolean rebooting, boolean rebootFailed) {
        Main.print(0,"Reboot set (network).");
        if (!segmentController.isOnServer()) {
            this.rebootCountdown = rebootCountdown;
            this.rebooting = rebooting;
            this.rebootFailed = rebootFailed;
            Main.print(0,"Countdown set to "+this.rebootCountdown);
        }
        else
            Main.print(0,"Reboot skipped (network).");
    }

    @Override
    public void sendNetworkUpdate() {
        if (!segmentController.isOnServer()) return;
        networkSemaphore = false;

        SSHPCCPacket packet = new SSHPCCPacket(segmentController.getId(), rebootCountdown, rebooting, rebootFailed);

        for (PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) {
            PacketUtil.sendPacket(player, packet);
            Main.print(0,"Reboot package sent. ("+rebootCountdown+"s, "+packet+")");
        }
    }

    @Override
    public void sendNetworkUpdatePrivate(PlayerState player) {
        PacketUtil.sendPacket(player,
                new SSHPCCPacket(segmentController.getId(), rebootCountdown, rebooting, rebootFailed));
    }

    @Override
    public void scheduleNetworkUpdate() {
        if (networkSemaphore || !segmentController.isOnServer()) return;
        networkSemaphore = true;
        Main.scheduleNetworkUpdate(this);
    }

    public void sendFullNetworkUpdatePrivate(PlayerState playerState) {
        sendNetworkUpdatePrivate(playerState);
        for (SSHPCounter counter : sshpCounters.values())
            counter.sendNetworkUpdatePrivate(playerState);
    }

    public SerialSSHPCC getSerial() {
        return new SerialSSHPCC(rebootCountdown, rebooting, rebootFailed, segmentControllerID, sshpCounters.values());
    }

    public String sshpsToString() {
        String string = "";
        for (SSHPCounter counter : sshpCounters.values()) {
            string = string +counter.blockID + ( counter.isDamaged ? "- " :": ") + counter.subSystemHP + ", ";
        }
        return "(" + string + ")";
    }
}
