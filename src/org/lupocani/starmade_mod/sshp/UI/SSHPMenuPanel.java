package org.lupocani.starmade_mod.sshp.UI;

import api.utils.gui.GUIMenuPanel;
import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.lupocani.starmade_mod.sshp.persist.SerialSSHPC;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIProgressBar;
import org.schema.schine.graphicsengine.forms.gui.GUITextButton;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIInnerTextbox;
import org.schema.schine.input.InputState;
import org.schema.schine.network.client.ClientState;

import java.util.ArrayList;
import java.util.Collections;

public class SSHPMenuPanel extends GUIMenuPanel {

    public static final short barHeight = 30;
    public static final short buttonHeight = 50;

    private GUITextButton button;

    private final ArrayList<SSHPGUIBar> sshpGUIBars = new ArrayList<>();

    public SSHPCounterController currentSSHPCC = null;
    public SSHPCounterController selectedSSHPCC = null;

    public SSHPMenuPanel(InputState inputState) {
        super(inputState, "SSHPMenuPanel", 800, 500);

        button = new GUITextButton(getState(), 0, buttonHeight, "Reboot Subsystems", new tempcallback());

        Main.print(0, "SSHP GUI instantiated.");
    }

    private void tabSetup(GUIContentPane tab, boolean remote) {
        tab.getTextboxes().get(0).setContent(button);
        tab.getTextboxes().get(0).setHeight(buttonHeight);

        for (SerialSSHPC counter : (!remote ? currentSSHPCC : selectedSSHPCC).getSerial().sshpCounters)
            if (counter.maxSSHP > 0)
                sshpGUIBars.add(new SSHPGUIBar(GameClientState.instance, 500, barHeight, counter, remote));

        sshpGUIBars.add(new SSHPGUIBar(GameClientState.instance, 500, barHeight, null, remote));
        Collections.sort(sshpGUIBars);

        for (SSHPGUIBar bar : sshpGUIBars){
            if (bar.remote != remote) continue;
            GUIInnerTextbox barTextBox = tab.addNewTextBox(0, barHeight);
            barTextBox.setContent(bar);
        }
    }

    @Override
    public void recreateTabs() {
        guiWindow.clearTabs();
        sshpGUIBars.clear();
        Main.print(0, "SSHP gui tabs re-created.");

        GUIContentPane firstTab = guiWindow.addTab((currentSSHPCC != null) ? "Entity SSHP Interface" : "N/A");
        firstTab.setTextBoxHeightLast(50);

        GUIContentPane secondTab = guiWindow.addTab((selectedSSHPCC != null) ? "Selected entity SSHP list" : "N/A");
        secondTab.setTextBoxHeightLast(50);

        /*
        if (currentSSHPCC == null) {
            Main.print(0, "No entity entered, leaving SSHP GUI blank.");
            return;
        }//*/

        if (currentSSHPCC != null)
            tabSetup(firstTab, false);
        if (selectedSSHPCC != null)
            tabSetup(secondTab, true);
    }

    public void update(SSHPCounterController controller, SSHPCounterController selected) {
        for (SSHPGUIBar bar : sshpGUIBars)
            if (bar.remote && selected != null)
                bar.update(selected);
            else if (!bar.remote && controller != null)
                bar.update(controller);

        if (controller == null) return;

        if (controller.isRebooting()) {
            String time = String.format("%.2f", controller.rebootCountdown);

            if (!controller.rebootFailed)
                button.setText("Subsystems rebooting... (" + time + ")");
            else
                button.setText("Reboot disrupted. Reset? (" + time + ")");
        }
        else
            button.setText("Reboot Subsystems");

        button.setActive(!controller.isRebooting() || controller.rebootFailed);
    }
}

class tempcallback implements GUICallback {
    @Override
    public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
        if (mouseEvent.pressedLeftMouse()) {
            Main.handleGUIReboot(GameClientState.instance.getPlayer());
        }
    }

    @Override
    public boolean isOccluded() {
        return false;
    }
}

class SSHPGUIBar extends GUIProgressBar implements Comparable<SSHPGUIBar> {
    public final short sysId;
    public final String name;
    public final boolean remote;

    public SSHPGUIBar(ClientState clientState, float v, float v1, final SerialSSHPC counter, boolean remote) {
        super(clientState, v, v1, new Object() {
            @Override
            public String toString() {
                if (counter != null)
                    return String.format("%s (x%.2f)", counter.systemName, counter.damageFactor);
                else
                    return "Total SSHP";
            }
        }, true, null);
        if (counter != null) {
            this.sysId = counter.blockID;
            this.name = counter.systemName;
        }
        else {
            this.sysId = -1;
            this.name = "";
        }
        this.setPercent(0);
        this.remote = remote;
    }

    public void update(SSHPCounterController controller) {
        if (sysId > 0)
            setPercent(controller.getSystemCapacity(sysId));
        else
            setPercent((float) controller.sshpccHealth());
    }

    @Override
    public int compareTo(SSHPGUIBar o) {
        return name.compareTo(o.name);
    }
}
