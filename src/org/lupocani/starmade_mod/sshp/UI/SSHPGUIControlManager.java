package org.lupocani.starmade_mod.sshp.UI;

import api.utils.gui.GUIControlManager;
import org.lupocani.starmade_mod.sshp.Main;
import org.lupocani.starmade_mod.sshp.SSHPCounterController;
import org.lupocani.starmade_mod.sshp.utils.SSHPUtils;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.graphicsengine.core.Timer;

public class SSHPGUIControlManager extends GUIControlManager {

    public SSHPGUIControlManager(GameClientState clientState) {
        super(clientState);
    }

    @Override
    public SSHPMenuPanel createMenuPanel() {
        Main.print(0,"SSHP GUI instantiating.");
        return new SSHPMenuPanel(getState());
    }

    @Override
    public SSHPMenuPanel getMenuPanel() {
        return (SSHPMenuPanel) super.getMenuPanel();
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            Main.print(0,"Opened GUI.");

            if (getMenuPanel() != null) {
                getMenuPanel().currentSSHPCC = Main.getClientPilotedSSHPCC();
                getMenuPanel().selectedSSHPCC = Main.getClientSelectedSSHPCC();

                getMenuPanel().recreateTabsSafe();
            }
            else {
                Main.print(0,"Could not instantiate GUI, no panel found.");
            }
        }
    }

    @Override
    public void update(Timer timer) {
        super.update(timer);
        if (getMenuPanel() == null) return;

        SSHPCounterController piloted = Main.getClientPilotedSSHPCC();

        SSHPCounterController selected = Main.getClientSelectedSSHPCC();

        getMenuPanel().update(piloted, selected);
    }
}